﻿alter table correntista
add column endereco varchar(100) null;

update correntista
set endereco = 'Rua x, 12'
where nm = 'Maria';

update  correntista
set endereco = 'Rua y, 567',
	nm = 'claudio'
where nm = 'Pedro';

select * 
from movimento;

select *
from correntista;

select *
from saldo;

create role r_correntista;

grant select, insert, update on movimento to r_correntista;
grant select on correntista to r_correntista;
grant select on saldo to r_correntista;
grant usage on movimento_id_seq to r_correntista;
grant insert,update on saldo to r_correntista;

revoke select on correntista from r_correntista;

grant select(id, nm) on correntista to r_correntista;

grant select on correntista to r_correntista;

create user claudio password 'caju123';

grant r_correntista to claudio;

create view v_correntista
as
	select 	id,
		nm,
		endereco
	from	correntista
	where	nm = current_user;

grant select on v_correntista to r_correntista;
