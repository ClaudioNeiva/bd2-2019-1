-- Criação das tabelas
create table correntista (
	id		serial		not null,
	nm		varchar(40)	not null,
	constraint pk_correntista
		primary key (id));

create table movimento (
	id		serial		not null,
	id_correntista	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(100)	    null,
	constraint pk_movimento
		primary key (id),
	constraint fk_movimento_correntista
		foreign key (id_correntista)
		references correntista
		on update cascade);

create table saldo (
	id_correntista	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	constraint pk_saldo 
		primary key (id_correntista, dt),
	constraint fk_saldo_correntista
		foreign key (id_correntista)
		references correntista
		on update cascade);

create or replace function f_trg_mov()
returns trigger
as
$$
declare saldo_anterior numeric;
begin
	if tg_op = 'INSERT' or tg_op = 'UPDATE' then
		if (select count(*) from saldo where id_correntista = new.id_correntista and dt = new.dt) = 0 then
			select 	s1.vl
			into	saldo_anterior
			from	saldo s1
			where	s1.id_correntista = new.id_correntista
			and	s1.dt = (select max(s2.dt)
					from	saldo s2
					where	s2.id_correntista = new.id_correntista
					and	s2.dt		  < new.dt);
			insert into saldo (id_correntista, dt, vl)
			values (new.id_correntista, new.dt, coalesce(saldo_anterior,0));
		end if;
		update 	saldo
		set	vl = vl + new.vl
		where 	id_correntista = new.id_correntista
		and	dt >= new.dt;
	end if;
	if tg_op = 'DELETE' or tg_op = 'UPDATE' then
		update 	saldo
		set	vl = vl - old.vl
		where 	id_correntista = old.id_correntista
		and	dt >= old.dt;
	end if;
	if tg_op = 'INSERT' or tg_op = 'UPDATE' then
		return new;
	else
		return old;
	end if;
end;
$$ language plpgsql;

create trigger trg_mov
before insert or delete or update of id_correntista, dt, vl
on movimento
for each row
execute procedure f_trg_mov();

insert into correntista (nm) 
values 
('Maria'),
('Pedro');

insert into movimento (id_correntista, dt, vl, ds_historico)
values 
((select id from correntista where nm ='Maria'),'2018-04-05',120,'Depósito salário'),
((select id from correntista where nm ='Maria'),'2018-04-05',-10,'Saque');

insert into movimento (id_correntista, dt, vl, ds_historico)
values 
((select id from correntista where nm ='Maria'),'2019-04-14',40,'Transferência');

insert into movimento (id_correntista, dt, vl, ds_historico)
values 
((select id from correntista where nm ='Maria'),'2018-04-07',-10,'Saque 1');

insert into movimento (id_correntista, dt, vl, ds_historico)
values 
((select id from correntista where nm ='Maria'),'2017-04-10',-50,'Saque 2');

delete 
from movimento
where 	id_correntista = (select id from correntista where nm ='Maria')
and	dt = '2018-04-05'
and	vl = -10;

update movimento
set	vl = 50
where 	id_correntista = (select id from correntista where nm ='Maria')
and	dt = '2018-04-01'
and	vl = 40;

delete
from saldo;

select * 
from correntista;

select *
from movimento
order by dt asc;

select *
from saldo
order by dt asc;
