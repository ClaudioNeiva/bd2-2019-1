﻿--Criação das tabelas
create table departamento (
        cod                     char(3)         not null,
        nome                    varchar(60)     not null,
        id_gerente              integer             null);
 
create table funcionario (
        id                      serial          not null,
        nome                    varchar(60)     not null,
        cpf                     char(11)        not null,
        endereco                varchar(60)         null,
        situacao                char(1)         not null,
        salario                 numeric(10,2)   not null,
        cod_departamento        char(3)             null);
 
create table dependente (
        id_funcionario          integer         not null,
        seq                     smallint        not null,
        nome                    varchar(60)     not null,
        grau_parentesco         char(1)         not null);

--1
alter table departamento 
	add constraint pk_departamento
		primary key (cod);
alter table funcionario
	add constraint pk_funcionario
		primary key (id);
alter table dependente
	add constraint pk_dependente
		primary key (id_funcionario, seq);
		
--2
alter table funcionario
	add constraint ck_funcionario_situacao
		check (situacao in ('A','D')),
	add constraint ck_funcionario_salario
		check (salario > 0);
alter table dependente
	add constraint ck_dependente_grau_parentesco
		check (grau_parentesco in ('F','C','O'));

--3 
alter table departamento
	add constraint fk_departamento_gerente
		foreign key (id_gerente)
		references funcionario
		on delete set null
		on update cascade;
alter table funcionario
	add constraint fk_funcionario_departamento
		foreign key (cod_departamento)
		references departamento
		on delete restrict
		on update cascade;
alter table dependente
	add constraint fk_dependente_funcionario
		foreign key (id_funcionario)
		references funcionario
		on delete cascade
		on update restrict;

--4
alter table funcionario
	alter column situacao set default 'A';
	
--5
alter table funcionario
	add constraint un_funcionario 
		unique (cpf);
		
--Inserts:
insert into departamento (cod,nome)
values 
('INF','Informática'),
('CTB','Contabilidade'),
('VND','Vendas'),
('LTC','Logística');
 
insert into funcionario (nome,cpf,endereco,salario,cod_departamento)
values
('Antonio',     '123','Rua X',  15800,  'INF'),
('Maria',       '456','Rua Y',  12000,  'INF'),
('Pedro',       '678',null,     7000,   'CTB'),
('Clara',       '135',null,     5000,   'CTB'),
('João',        '246','Rua K',  6500,   null),
('Carlos',      '357','Rua L',  12000,  'VND'),
('Joaquim',     '468','Rua Y',  8000,   'CTB');

insert into dependente (id_funcionario,seq,nome,grau_parentesco)
values
((select id from funcionario where nome = 'Antonio'),1,'Mateus','F'),
((select id from funcionario where nome = 'Antonio'),2,'Manuela','F'),
((select id from funcionario where nome = 'Antonio'),3,'Leila','C'),
((select id from funcionario where nome = 'Clara'),1,'Claudio','C'),
((select id from funcionario where nome = 'Clara'),2,'Bruna','F'),
((select id from funcionario where nome = 'Carlos'),1,'Vera','C');

update  departamento
set id_gerente = (select id from funcionario where nome = 'Antonio')
where   cod = 'INF';
update  departamento
set id_gerente = (select id from funcionario where nome = 'Joaquim')
where   cod = 'CTB';
update  departamento
set id_gerente = (select id from funcionario where nome = 'Carlos')
where   cod = 'VND';

--6. O nome e endereço dos funcionários do departamento de Informática, por ordem de nome.
select 	nome,
	endereco
from	funcionario
where	cod_departamento = 'INF'
order by nome desc;

select 	f.nome,
	f.endereco
from	funcionario f
	inner join departamento d
		on (d.cod = f.cod_departamento)
where	d.nome = 'Informática'
order by f.nome desc;

--NÃO FAÇA ASSIM, POR FAVOR!! - INÍCIO
--NÃO FAÇA ASSIM, POR FAVOR!!
--NÃO FAÇA ASSIM, POR FAVOR!!
--NÃO FAÇA ASSIM, POR FAVOR!!
--NÃO FAÇA ASSIM, POR FAVOR!!
select 	f.nome,
	f.endereco
from	funcionario f,
	departamento d
where	d.nome = 'Informática'
and 	d.cod = f.cod_departamento
order by f.nome desc;
--NÃO FAÇA ASSIM, POR FAVOR!! - FIM


--7. A lista de departamentos (código e nome) e seus respectivos funcionários (nome), 
-- ordenado por nome de departamento e nome do funcionário. 
-- Observações: os departamentos que não tiverem funcionários, na coluna de nome do funcionário 
-- deve aparecer a mensagem: (Nenhum funcionário alocado neste departamento)

select	d.cod as cod_departamento,
	d.nome as nome_departamento,
	coalesce(f.nome,'(Nenhum funcionário alocado neste departamento)') as nome_funcionario
from	departamento d	
	left outer join funcionario f
		on (f.cod_departamento = d.cod)
order by d.nome,
	 f.nome;

--8. Os departamentos (código e nome) e suas respectivas características salariais: maior salário, menor salário e salário médio. 
-- Somente devem ser utilizados funcionários com situação Ativo, no cálculo.
select	d.cod as cod_departamento,
	d.nome as nome_departamento,
	max(f.salario) as maior_salario,
	min(f.salario) as menor_salario,
	avg(f.salario) as salario_medio,
	cast(avg(f.salario) as numeric(10,2)) as salario_medio_arredondado_2casas
from	departamento d
	inner join funcionario f
		on (f.cod_departamento = d.cod)
where	f.situacao = 'A'
group by d.cod,
	d.nome;

--9. Os nomes dos departamentos e dos funcionários. Suas listagem devem incluir departamentos sem funcionários e 
--funcionários sem departamento.
select	d.nome as nome_departamento,
	f.nome as nome_funcionario
from	departamento d
	full outer join funcionario f
		on (f.cod_departamento = d.cod);

--10. Os nomes e endereços dos funcionários e dos dependentes. A lista será utilizada para emissão de uma mala direta, 
-- logo sua consulta deverá retornar apenas duas colunas: nome e endereço.
select	f.nome,
	f.endereco
from	funcionario f
where	f.endereco is not null
union
select	d.nome,
	f.endereco
from	dependente d
	inner join funcionario f
		on (f.id = d.id_funcionario)
where	f.endereco is not null;

--11. Os departamentos (código e nome) cuja média salarial seja superior à 10000.
select	d.cod as cod_departamento,
	d.nome as nome_departamento
from	departamento d
	inner join funcionario f
		on (f.cod_departamento = d.cod)
group by d.cod,
	 d.nome
having avg(salario) > 10000;

--12. Todas as combinações possíveis de nome de departamento e nome de funcionário.
select	d.nome as nome_departamento,
	f.nome as nome_funcionario
from	departamento d
	cross join funcionario f;
	
--13. Nomes dos funcionários que possuem dependentes
select	f.nome 
from 	funcionario f
where	f.id in (select distinct d.id_funcionario
			from	dependente d);

select	distinct f.nome 
from	dependente d
	inner join funcionario f
		on (f.id = d.id_funcionario);

--14. Crie visões com as seguintes características: v_funcionarios_ativos: 
-- código e nome do departamento, id, nome, endereço, cpf e salário dos funcionários ativos.
create view v_funcionarios_ativos as
	select	d.cod as cod_departamento, 	
		d.nome as nome_departamento,  
		f.id as id_funcionario,  
		f.nome as nome_funcionario, 
		f.endereco as endereco_funcionario,  
		f.cpf as cpf_funcionario ,
		f.salario as salario_funcionario
	from	departamento d
		inner join funcionario f
			on (f.cod_departamento = d.cod)
	where	f.situacao = 'A';

create MATERIALIZED view v_funcionarios_ativos_MATERIALIZED as
	select	d.cod as cod_departamento, 	
		d.nome as nome_departamento,  
		f.id as id_funcionario,  
		f.nome as nome_funcionario, 
		f.endereco as endereco_funcionario,  
		f.cpf as cpf_funcionario ,
		f.salario as salario_funcionario
	from	departamento d
		inner join funcionario f
			on (f.cod_departamento = d.cod)
	where	f.situacao = 'A';

select	count(*) 
from v_funcionarios_ativos;

insert into funcionario (nome,cpf,endereco,salario,cod_departamento)
values
('Ana',     '830','Rua P',  23000,  'INF');

select	count(*) 
from v_funcionarios_ativos;

select	count(*) 
from v_funcionarios_ativos_MATERIALIZED;

refresh materialized view v_funcionarios_ativos_MATERIALIZED;

select	count(*) 
from v_funcionarios_ativos_MATERIALIZED;

select	count(*)
from v_funcionarios_ativos
where	cod_departamento = 'INF';

--15. Crie visões com as seguintes características: v_dependentes: id, nome, endereço dos funcionários e nomes de seus dependentes
create view v_dependentes as
	select	f.id as id_funcionario,
		f.nome as nome_funcinario,
		f.endereco as endereco_funcionario,
		d.nome as nome_dependente
	from	funcionario f
		inner join dependente d 
			on (d.id_funcionario = f.id);

select 	*
from	v_dependentes;			

--16. Torne obrigatórios os campos de id_gerente da tabela de departamentos e cod_departamento da tabela de funcionários. 
-- Para que seja possível realizar esta alteração, deve ser deletado o departamento Logística e os funcionários 
-- que não estão alocados em nenhum departamento
delete 
from departamento
where id_gerente is null;

delete 
from funcionario
where cod_departamento is null;
 
alter table departamento
	alter column id_gerente set not null;

alter table funcionario
	alter column cod_departamento set not null;

--17. Insira o departamento Logística, definindo como seu gerente a funcionária 
-- Isabel (cpf 776, moradora da Rua P, com salário de 9600).
-- Para realizar esta tarefa pesquise: sobre 'deferrable initially immediate' e 'set constraints'.

alter table departamento
	drop constraint fk_departamento_gerente;
	
alter table funcionario
	drop constraint fk_funcionario_departamento;
	
alter table departamento
	add constraint fk_departamento_gerente
		foreign key (id_gerente)
		references funcionario
		on update cascade
		deferrable initially immediate;
		
alter table funcionario
	add constraint fk_funcionario_departamento
		foreign key (cod_departamento)
		references departamento
		on delete restrict
		on update cascade
		deferrable initially immediate;

-- No caso abaixo, a inconsistência do insert de Isabel, que estava lotada em LTC, ainda inexistente, foi resolvida antes do 
-- commit. Ao final da transação o banco estava num estado consistente.

begin

set constraints fk_departamento_gerente, fk_funcionario_departamento deferred;
 
insert into funcionario (nome,cpf,endereco,salario,cod_departamento)
values ('Isabel',     '776','Rua P',  9600,  'LTC');

insert into departamento (cod,nome,id_gerente)
values ('LTC','Logística',(select id from funcionario where nome = 'Isabel'));

commit;

-- No caso abaixo, a inconsistência do insert de Caju, que estava lotado em MNG, ainda inexistente, NÃO foi resolvida antes do 
-- commit. Ao final da transação o banco indicou a violação de chave estrangeira e fez o rollback.
begin

set constraints fk_departamento_gerente, fk_funcionario_departamento deferred;

insert into funcionario (nome,cpf,endereco,salario,cod_departamento)
values ('Caju',     '111776','Rua P',  9600,  'MNG');

commit;

