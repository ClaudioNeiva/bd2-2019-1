-- EXCLUIR TABELAS
DROP TABLE IF EXISTS veiculo CASCADE;
DROP TABLE IF EXISTS veiculo_acessorio CASCADE;
DROP TABLE IF EXISTS acessorio CASCADE;
DROP TABLE IF EXISTS modelo CASCADE;
DROP TABLE IF EXISTS grupo CASCADE;
DROP TABLE IF EXISTS preco CASCADE;
DROP TABLE IF EXISTS fabricante CASCADE;
DROP TABLE IF EXISTS cidade CASCADE;
DROP TABLE IF EXISTS estado CASCADE;
DROP TYPE IF EXISTS t_endereco CASCADE;



-- TABLES

CREATE TABLE veiculo(
    nu_placa			CHAR(7)			NOT  NULL,
    nu_chassi			CHAR(17)		NOT  NULL,
    nu_km 			INT			NOT  NULL,
    vl_ano_fab 			SMALLINT		NOT  NULL,
    dt_aquisicao		DATE			NOT  NULL,    
    dt_venda 			DATE			     NULL,
    cd_modelo			INT			NOT  NULL,
    cd_grupo			INT		 	NOT  NULL,
    constraint pk_veiculo
	    primary key(nu_placa),
    constraint un_veicul_chassi
    	unique (nu_chassi)
);

CREATE TABLE veiculo_acessorio(
    sg_acessorio		VARCHAR(5)		NOT  NULL,
    nu_placa_veiculo		CHAR(7)			NOT  NULL,
    constraint pk_veiculo_acessorio
	    primary key(sg_acessorio, nu_placa_veiculo)
);

CREATE TABLE acessorio(
    sg				VARCHAR(5)		NOT  NULL,
    nm				VARCHAR(50)		NOT  NULL,
    constraint pk_acessorio
    	primary key (sg)
);
    
CREATE TABLE modelo(
    cd				INT 			NOT  NULL,
    nm				VARCHAR(50)		NOT  NULL,
    nu_cnpj_fabricante		CHAR(14)		NOT  NULL,
    constraint pk_modelo
    	primary key (cd)
);
    
CREATE TABLE grupo (
    cd 				SERIAL			NOT  NULL,
    nm				VARCHAR(15)		NOT  NULL,
    constraint pk_grupo
    	primary key(cd)
);

CREATE TABLE preco(
    cd_grupo			INTEGER			NOT NULL,
    dt_venda			DATE			NOT NULL,
    valor			NUMERIC(10,2)		NOT NULL,
    constraint pk_preco
    	primary key(cd_grupo, dt_venda)
);

CREATE TYPE t_endereco AS (
    nm_bairro			VARCHAR(50),
    nm_logradouro		VARCHAR(100),
    nu				CHAR(5),
    nm_complemento		VARCHAR(50)
);

CREATE TABLE fabricante(
    nu_cnpj			CHAR(14)		NOT NULL,
    nm		VARCHAR(30)		NOT NULL,
    nu_telefone			CHAR(14)		NULL,
    endereco			t_endereco		NOT NULL,
    sg_estado			char(2)			NOT NULL,
    sg_cidade			char(3)			NOT NULL,
    constraint pk_fabricante
    	primary key(nu_cnpj)
);

CREATE TABLE estado(
    sg				CHAR(2) 		NOT NULL,
    nm				VARCHAR(50)		NOT NULL,
    constraint pk_estado
    	primary key(sg)
);    

CREATE TABLE cidade(
    sg_estado			CHAR(2)			NOT NULL,
    sg				CHAR(3)			NOT NULL,
    nm				VARCHAR(50)		NOT NULL, 
    constraint pk_cidade
	primary key(sg_estado, sg)
);
          
alter table veiculo_acessorio
	add constraint fk_veiculo_acessorio_veiculo
		foreign key (nu_placa_veiculo)
		references veiculo
		on delete cascade
		on update cascade,
	add constraint fk_veiculo_acessorio_acessorio
		foreign key (sg_acessorio)
		references acessorio
		on update cascade;

alter table veiculo
	add constraint fk_veiculo_modelo
		foreign key (cd_modelo)
		references modelo,
	add constraint fk_veiculo_grupo
		foreign key (cd_grupo)
		references grupo;

alter table modelo
	add constraint fk_modelo_fabricate
		foreign key (nu_cnpj_fabricante)
		references fabricante
		on update cascade;

alter table preco	
	add constraint fk_preco_grupo
		foreign key (cd_grupo)
		references grupo;

alter table fabricante
	add constraint fk_fabricante_cidade
		foreign key (sg_estado, sg_cidade)
		references cidade;

alter table cidade
	add constraint fk_cidade_estado
		foreign key (sg_estado)
		references estado;

-- on update / on delete: restrict (default), cascade, set null

-- Parte B
-- Elabore as seguintes consultas:

-- Dados para teste
insert into estado (sg, nm)
values
('BA','Bahia'),
('SP','São Paulo'),
('RJ','Rio de Janeiro'),
('SE','Sergipe');

insert into cidade (sg_estado, sg, nm)
values 
('BA','CAM','Camaçari'),
('BA','SSA','Salvador'),
('SP','SPO','São Paulo'),
('RJ','RJO','Rio de Janeiro'),
('SE','ARJ','Aracajú');

insert into fabricante(nu_cnpj, nm, nu_telefone, endereco, sg_estado,sg_cidade)
values 
('234','Honda','456',('Cajazeiras 999','Rua 222','23','Algum lugar perto de outro lugar muito distante'),'BA','SSA'),
('123','VW','113',('Cajazeiras','Rua 1000','23',''),'BA','SSA'),
('345','GM','114',('Moema','Rua x','12',''),'SP','SPO'),
('456','Fiat','225',('Ipanema','Rua y','245',''),'RJ','RJO'),
('567','Ford','226',('Bomba','Rua z','2312',''),'BA','CAM');

-- 1. Listar veículos: placa e idade
select	nu_placa as placa,
	DATE_PART('YEAR',CURRENT_DATE) - vl_ano_fab as idade
from	veiculo
;
	
-- 2. Listar veículos: todos os atributos
-- Filtro: fabricados antes de 2015
select	*
from	veiculo
where	vl_ano_fab < 2015
;

-- 3. Listar veículos: todos os atributos
-- Filtro: adquiridos antes de 2015 e com quilometragem maior que 40000
select	*
from	veiculo
where	vl_ano_fab < 2015 
and	nu_km > 40000 
;

-- 4. Listar modelos: todos os atributos
-- Filtro: nome contem a palavra "gol" em qualquer local
select	*
from	modelo
where	nm ilike ('%gol%')
;

-- 5. Listar fabricante:
-- Campos: composição do endereço; na composição de endereço (logradouro todo em
-- minúsculo, primeiros 10 caracteres do complemento); nome do fabricante todo em
-- maiúsculo;
select	nu_cnpj, 
	upper(nm), 
	nu_telefone, 
	(endereco).nm_bairro, 
	lower((endereco).nm_logradouro), 
	(endereco).nu, 
	substr((endereco).nm_complemento, 1, 10), 
	sg_estado, 
	sg_cidade
from	fabricante;

-- Filtro: cnpj com menos de 14 caracteres (lembre-se de remover os caracteres em
-- branco do início e fim do campo durante a consulta) ou que possuam caracteres não
-- numéricos

-- 6. Listar veículos:
-- Campos (agrupamento): quantidade; quilometragem do mais rodado; quilometragem
-- do menos rodado; quilometragem média; total de quilômetros rodados por todos os
-- veículos

-- 7. Listar veículos:
-- Campos: ano de fabricação
-- Campos (agrupamento): quantidade; quilometragem do mais rodado; quilometragem
-- do menos rodado; quilometragem média; total de quilometros rodados por todos os
-- veículos