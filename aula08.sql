-- Exclusão das tabelas
drop table if exists correntista cascade;
drop table if exists movimento cascade;
drop table if exists saldo cascade;


-- Criação das tabelas
create table correntista (
	id		serial		not null,
	nm		varchar(40)	not null,
	constraint pk_correntista
		primary key (id));

create table movimento (
	id		serial		not null,
	id_correntista	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(100)	    null,
	constraint pk_movimento
		primary key (id),
	constraint fk_movimento_correntista
		foreign key (id_correntista)
		references correntista
		on update cascade);

create table saldo (
	id_correntista	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	constraint pk_saldo 
		primary key (id_correntista, dt),
	constraint fk_saldo_correntista
		foreign key (id_correntista)
		references correntista
		on update cascade);

-- Criação da função que será disparada pelo trigger

create or replace function f_trg_correntista()
returns trigger
as $$
begin
	raise notice 'Id=%, Nome=%', NEW.id, NEW.nm;
	return NEW;
end;
$$ language plpgsql;


-- Criação do trigger 

create trigger trg_correntista
before insert
on correntista
for each row
execute procedure f_trg_correntista();


-- Teste do trigger

insert into correntista (nm) 
values 
('Antonio Claudio'),
('Maria da Silva');

select *
from correntista;


-- Alteração a função do trigger para "impedir" o insert. 
-- Ao retornar "null" para a função do trigger, o insert foi "abortado", sem o cancelamento da transação.

create or replace function f_trg_correntista()
returns trigger
as $$
begin
	raise notice 'Id=%, Nome=%', NEW.id, NEW.nm;
	return null;		-- "NUNCA" FAÇA ISSO. POIS A TRANSAÇÃO NÃO FOI ABORTADA E NEM O REGISTRO FOI GRAVADO!
end;
$$ language plpgsql;


-- Teste do trigger, após a alteração da função do trigger

insert into correntista (nm) 
values 
('Ana'),
('Clara');

select *
from correntista;


-- Alteração da função trigger para fazer uso de variáveis associadas ao trigger;

create or replace function f_trg_correntista()
returns trigger
as $$
begin
	if TG_OP ='INSERT' then
		raise notice 'Aqui vou colocar o código que vai rodar no insert.';
	end if;
	raise notice 'Id=%, Nome=%', NEW.id, NEW.nm;
	return NEW;
end;
$$ language plpgsql;


-- Teste do trigger, após a alteração da função do trigger

insert into correntista (nm) 
values 
('Ana'),
('Clara');

select *
from correntista;


-- Alteração da função trigger para refletir a associação à diferentes eventos;

create or replace function f_trg_correntista()
returns trigger
as $$
begin
	if TG_OP ='INSERT' then
		raise notice 'Aqui vou colocar o código que vai rodar no insert.';
    	return NEW;
	elsif TG_OP ='UPDATE' then
		raise notice 'Aqui vou colocar o código que vai rodar no update.';
    	return NEW;
	elsif TG_OP ='DELETE' then
		raise notice 'Aqui vou colocar o código que vai rodar no delete.';
	    return OLD;
	elsif TG_OP ='TRUNCATE' then
		raise notice 'Aqui vou colocar o código que vai rodar no truncate.';
    	return OLD;
	end if;
end;
$$ language plpgsql;


-- Alteração do trigger. Como não dispomos de um create or replace, para alterar o trigger
-- é necessário excluir e criar outra vez.

drop trigger if exists trg_correntista on correntista;

create trigger trg_correntista
before insert or update or delete 
on correntista
for each row
execute procedure f_trg_correntista();


-- Teste do trigger, após a alteração da função do trigger e do trigger

insert into correntista (nm) 
values 
('Pedro'),
('João');

select *
from correntista;


-- Como a função do trigger faz uso incondicional do registro NEW, como no disparo
-- com o delete esse registro não existe, o disparo do trigger, com a execução do delete, dará erro!

delete 
from correntista
where id < 5;

select *
from correntista;


-- Alteração da função trigger, que dispara para eventos de insert, delete e update, para permitir o uso adequado
-- do NEW e do OLD.

create or replace function f_trg_correntista()
returns trigger
as $$
begin
	raise notice 'Usuário=% | data=%',CURRENT_USER, CURRENT_TIMESTAMP;
	if TG_OP ='INSERT' then
		raise notice 'INSERT -> Estado posterior -> Id=%, Nome=%', NEW.id, NEW.nm;
	    return NEW;
	elsif TG_OP ='UPDATE' then
		raise notice 'UPDATE  -> Estado anterior -> Id=%, Nome=%', OLD.id, OLD.nm;
		raise notice 'UPDATE  -> Estado posterior -> Id=%, Nome=%', NEW.id, NEW.nm;
    	return NEW;
	elsif TG_OP ='DELETE' then
		raise notice 'DELETE  -> Estado posterior -> Id=%, Nome=%', OLD.id, OLD.nm;
    	return OLD;
	end if;
end;
$$ language plpgsql;


-- Teste do trigger para eventos de insert, update e delete.

delete 
from correntista
where id < 5;

update correntista
set nm = 'Hugo'
where id = 7;

insert into correntista (nm) 
values 
('Victor');

select *
from correntista;
